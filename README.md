## Fork the Assignment 0 repository. ##
In BitBucket, select the CS5700 Assignment 0 Repository. Use the panel on the left side of the screen to fork it. 

After you have forked the repository, use the panel on the left side of the screen to clone it. This will pop up a box with a URL that looks like “git clone https://cjthatcher_academic@bitbucket.org/cjthatcher_academic/cs5700.git”. Copy that.

## Clone the forked repository onto your disk. ## 
### Eclipse Version: ###
   In Eclipse, go to the Project Explorer area. 
   Right Click -> Import -> Git -> Projects from Git -> Clone URI
   Paste the URL that BitBucket gave you when you forked the repo. Remove the “git clone” part of it, and paste it into the URI section of the form
   Put in your password, then hit next a million times.

### SourceTree Version: ###
   Open up SourceTree
   Click on Clone / New in the upper-left corner
   Paste in the URL you got from BitBucket when you forked the repo, except remove the “git clone” portion of it.
   Choose where to put it on disk.


In Eclipse, go to the Project Explorer section
Right Click -> Import -> Git -> Projects from Git -> Existing Local Repository -> “Add” -> Navigate to the place on disk where you cloned the repository.    Select the project and click next a billion times

Open the project you just imported into Eclipse. You will see some existing code, as well as some instructions in the comments. Follow the instructions, make necessary code changes, and then commit and push your changes.

To commit your changes, you will need to open the “Git Staging” view in Eclipse.
Go to Window -> Show View -> Other -> Git -> Git Staging
Note: The Git Staging view shows you what code has changed locally before you commit it.
Move your changed files from “Unstaged Changes” to “Staged Changes”
Write a good commit message, then click “Commit and Push”
This will commit your changes, and then push them up to BitBucket.
Note: If you just hit “Commit” instead of “Commit and Push”, you can still push. Right click on your project -> Team -> Push to Upstream

Now that your changes are up on BitBucket, make a Pull Request. This is an official request for me to merge your changes back into the original repository. I won’t merge them in, but it’s a great chance for us all to review the code. Add myself (christopher.thatcher@aggiemail.usu.edu) as well as our loyal grader (laleh.rostami@aggiemail.usu.edu) as reviewers.
