package edu.usu.cs.oo;

import edu.usu.cs.oo.Company;
import edu.usu.cs.oo.Job;
import edu.usu.cs.oo.Location;
import edu.usu.cs.oo.Student;

public class Driver {
	
	public static void main(String[] args)
	{
		//Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		Student me = new Student("Dan Scannell","A00393996",new Job("Recording Artist", 150000, new Company("Nintendo",new Location("La Roda", "90210","Cupertino","California"))));
		
		System.out.println(me);
		
		/*
		 * Print out the student information. 
		 */
		me.toString();
	}

}